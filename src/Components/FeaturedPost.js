import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Hidden from "@material-ui/core/Hidden";

import moment from "moment";

const useStyles = makeStyles({
  card: {
    display: "flex",
  },
  cardDetails: {
    flex: 1,
  },
  cardMedia: {
    width: 160,
  },
});

export default function FeaturedPost(props) {
  const classes = useStyles();
  const {
    by,
    descendants,
    id,
    kids,
    score,
    time,
    title,
    type,
    url,
  } = props?.post;

  return (
    <Grid item xs={12} md={12}>
      <CardActionArea component='a' href={url} target='black'>
        <Card className={classes.card}>
          <div className={classes.cardDetails}>
            <CardContent>
              <Typography component='h2' variant='h5'>
                {title}
              </Typography>
              <Typography variant='subtitle1' color='textSecondary'>
                ({url})
              </Typography>
              <Grid
                container
                direction='row'
                justify='space-between'
                alignItems='center'
              >
                <Grid item>
                  <Typography variant='caption' color='primary'>
                    {score} points | {descendants} comments
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant='caption' color='textSecondary'>
                    by {by}, {moment(time * 1000).format("ll")}
                  </Typography>
                </Grid>
              </Grid>
            </CardContent>
          </div>
        </Card>
      </CardActionArea>
    </Grid>
  );
}

FeaturedPost.propTypes = {
  post: PropTypes.object,
};
